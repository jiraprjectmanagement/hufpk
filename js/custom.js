
//   Home Page JS

$(function () {
    // Float Button
    $(".click-arrow").click(function(){
        $(".click-arrow").parent().parent().addClass("drop-sub-close");
        $(".click-arrow").parent().parent().removeClass("drop-sub-open");
    });
	$(".give-now").click(function(){
        $(".give-now + .drop-sub").addClass("drop-sub-open");
        // $(".give-now + .drop-sub").removeClass("drop-sub-close");
    });
    // Float Button



    $('#audio-control').click(function(){
        if( $("#myVideo").prop('muted') ) {
              $("#myVideo").prop('muted', false);
              $('#audio-control').addClass('set-mute');
            //   $(".set-mute").wrap('<i class="fas fa-volume-up"></i>');
          // or toggle class, style it with a volume icon sprite, change background-position
        } else {
          $("#myVideo").prop('muted', true);
          $('#audio-control').removeClass('set-mute');

        //   (this).empty();
        //   $(".set-mute").wrap('<i class="fas fa-volume-mute"></i>');
        }
    });


     $("#set-mute").replaceWith('<i class="fas fa-volume-mute"></i>');
    
        $('.read-more').on('click', function(){
            $('.read-more').toggleClass('current');
        });
    
      $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        centerMode: true,
        speed: 1000,
        variableWidth: true,
        // fade: true,
        asNavFor: '.slider-nav',
        // responsive: [
        //     {
        //       breakpoint: 768,
        //       settings: {
        //         slidesToShow: 1
        //       }
        //     },
        //     {
        //       breakpoint: 480,
        //       settings: {
        //         slidesToShow: 1
        //       }
        //     }
        //   ]
      });
      $('.slider-nav').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        speed: 1000,
        centerMode: true,
        focusOnSelect: true,
        responsive: [
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 3
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 3
              }
            }
          ]
      });
    
    

$('.counter-txt').countUp();


$('.main-banner').owlCarousel({
    loop:false,
    nav: true,
    navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
    dots: false,
    margin:30,
    autoplay:true,
    autoPlaySpeed: 5000,
    autoplayTimeout:5000,
    slideSpeed : 5000,
    autoplayHoverPause:false,
    mouseDrag: true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            dots: false,
        },                   
        576:{
            items:1,
            dots: false,
        },
        766: {
            items: 1,
            dots: false,
        }, 
        992: {
            items: 1,
            dots: false,
        }
    }
});


$('.community-slider').owlCarousel({
    loop:false,
    nav: true,
    navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
    dots: false,
    margin:30,
    stagePadding: 5,
    autoplay:false,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
    mouseDrag: true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav: true,
            dots:false
        },                   
        576:{
            items:2,
            nav: true,
            dots:false
        },
        768: {
            items: 2,
            dots: false,
            nav: true,
            margin:30,
        }, 
        992: {
            items: 2,
            dots: false,
            nav: true,
            margin:30,
        }, 
        1199: {
            items: 4,
            dots: false
        }
    }
});

if($(window).width() >= 576){
    $(".side-nav-tab .collapse-bar").addClass("show")
}
// $("li.nav-item.dropdown.dropdown-6").click(function(){
//     $("ul.dropdown-menu.dropdown_menu--animated.dropdown_menu-6").hide();
// });

// if($(window).width() <= 768){
//     if(('.nam-row-margin').length != 0){
//         $('.nam-row-margin').addClass('owl-carousel owl-theme');
//         $('.nam-row-margin').owlCarousel({
//             loop:false,
//             margin:0,
//             nav:false,
//             navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
//             dots: true,
//             autoplay:false,
//             autoplayTimeout:2000,
//             autoplayHoverPause:false,
//             mouseDrag: true,
//             responsive:{
//                 0:{

//                     items:1
//                 }, 
//                 480:{

//                     items:1
//                 },
//                 574:{

//                     items:2
//                 },
//                 767:{
//                     items:2
//                 }
//             }
//         });
//     }
// }


$(window).scroll(function(){
    var scrollheader  = $(window).scrollTop() > 50;
    
    if(scrollheader){
        $(".main-header").addClass("header-sticky");
    }
    else{
        $(".main-header").removeClass("header-sticky");
    }
   
});

$(window).scroll(function(){
    var scrollheader  = $(window).scrollTop() > 500;
    // console.log(scrollheader);
    if(scrollheader){
        $(".load-anim").addClass("anima-loa-active");
        // $(".count-info h3").removeClass("deflt-spin");
        
    }
    else{
        $(".load-anim").removeClass("anima-loa-active");
        // $(".count-info h3").addClass("deflt-spin");
    }
});


$(document).ready(function(){
	$('#nav-icon3').click(function(){
		$(this).toggleClass('open');
	});
});


AOS.init({
    disable: function() {
        return window.innerWidth < 800;
    }
});

if($(window).width() <= 575){
    $('#gridview').addClass('active');
    $('#sliderview').removeClass('active');
}
});

